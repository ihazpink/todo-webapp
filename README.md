# Forum

A todo website built in rust with actix-web, sqlx and tokio.

## Development (Flakes)

This repo uses [Flakes](https://nixos.wiki/wiki/Flakes) from the get-go, but
compat is provided for traditional nix-shell/nix-build as well
(see the section below).

```bash
# Dev shell
nix develop

# or run via cargo
nix develop -c cargo run

# build
nix build
```

## Development (Legacy Nix)

```bash
# Dev shell
nix-shell

# run via cargo
nix-shell --run 'cargo run'

# build
nix-build
```

There are also a couple of useful scripts:
- `bin/run` which starts 'cargo watch'; and it is used by VSCode as well 
  (`Ctrl+Shift+B`)
- `/bin/postgres` is used for managing a transient postgres container for
  development purposes

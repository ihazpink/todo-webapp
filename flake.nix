{
  description = "todo webapp build with rust and astro";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    devshell = { 
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nci = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.parts.follows = "flake-parts";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = inputs @ { flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = ["x86_64-linux"];
      imports = [
        inputs.devshell.flakeModule
        inputs.nci.flakeModule
      ];

      perSystem = {pkgs, config, ...}:
      let
        outputs = config.nci.outputs;
      in {
        nci = {
          projects."todo-webapp" = {
            relPath = "";
            export = true;

            depsOverrides = {
              add-inputs.overrideAttrs = old: {
                buildInputs = with pkgs; (old.buildInputs or []) ++ [
                  pkg-config
                  openssl
                ];
              };
            };
          };
          crates = {
            "todo-front" = {};
            "todo-api" = {};
          };
        };

        devshells.default = {
          imports = [
            "${inputs.devshell}/extra/language/rust.nix"
            "${inputs.devshell}/extra/language/c.nix"
          ];

          # Both of these are currently needed because the redis crate uses
          # openssl and not rustls.
          language.c.includes = [pkgs.openssl];
          packages = with pkgs; [pkg-config rnix-lsp];

          commands = with pkgs; let
            create-script = {
              name,
              runtimeInputs,
            }: let
              script =
                (pkgs.writeScriptBin name
                  (builtins.readFile ./bin/${name}))
                .overrideAttrs (old: {
                  buildCommand = "${old.buildCommand}\n patchShebangs $out";
                });
            in
              pkgs.symlinkJoin {
                inherit name;
                paths = [script] ++ runtimeInputs;
                buildInputs = [pkgs.makeWrapper];
                postBuild = "wrapProgram $out/bin/${name} --prefix PATH : $out/bin";
              };
            postgres-script = create-script {
              name = "postgres";
              runtimeInputs = with pkgs; [docker-client postgresql sqlx-cli];
            };
            redis-script = create-script {
              name = "redis";
              runtimeInputs = [pkgs.docker-client];
            };
          in [
            {
              package = cargo-watch;
              category = "rust";
            }
            {
              package = xh;
              category = "utilites";
            }
            {
              package = tokei;
              category = "utilites";
            }
            {
              package = bunyan-rs;
              category = "utilites";
            }
            {
              package = lazygit;
              category = "utilites";
            }
            {
              package = clippy;
              category = "rust";
            }
            {
              package = rustfmt;
              category = "rust";
            }
            {
              package = postgres-script;
              category = "scripts";
              help = "Script for managing a transient postgres container";
            }
            {
              package = redis-script;
              category = "scripts";
              help = "Script for managing a transient redis container";
            }
            {
              package = cargo;
              category = "rust";
            }
            {
              package = cargo-audit;
              category = "rust";
            }
            {
              package = nodejs;
              category = "front";
            }
            {
              package = just;
              category = "utilites";
            }
          ];

          env = [
            {
              name = "POSTGRES_USER";
              eval = "postgres";
            }
            {
              name = "POSTGRES_PASSWORD";
              eval = "mysupersecretpassword";
            }
            {
              name = "POSTGRES_DB";
              eval = "todo-dev";
            }
            {
              name = "POSTGRES_PORT";
              eval = "5432";
            }
            {
              name = "POSTGRES_IMAGE";
              eval = "postgres:14-alpine";
            }
            {
              name = "POSTGRES_CONTAINER_NAME";
              eval = "todo-postgres-dev";
            }
            {
              name = "DATABASE_URL";
              eval = ''
                postgres://$POSTGRES_USER:$POSTGRES_PASSWORD\
                @localhost:$POSTGRES_PORT/$POSTGRES_DB'';
            }
            {
              name = "REDIS_PORT";
              eval = "6379";
            }
            {
              name = "REDIS_CONTAINER_NAME";
              eval = "todo-redis-dev";
            }
            {
              name = "REDIS_IMAGE";
              eval = "redis:7-alpine";
            }
          ];
        };

       packages.default = outputs."todo-front".packages.release;
      }; # end of perSystem
    }; # end of flake-parts.lib.mkFlake
}

default:
	just --list

# Builds and runs the todo-front crate
run-front: build-front
	cd todo-front && cargo r

########## Build ##########

# Builds all crates in debug mode
build: build-front build-api

# Builds the todo-front crate in debug mode
build-front: clean-astro build-astro
	cd todo-front && cargo build

# Builds the todo-api crate in debug mode
build-api:
	cd todo-api && cargo build

# Builds the sailfish templates using astro
build-astro: clean-astro && _change-template-extensions
	cd todo-front/astro && npm run build

	mkdir -p todo-front/templates
	mv todo-front/astro/dist/*.html todo-front/templates

	mkdir -p todo-front/public
	mv  todo-front/astro/dist/* todo-front/public

_change-template-extensions:
	#!/usr/bin/env bash
	cd todo-front/templates
	for file in *.html; do
		mv -- "$file" "${file%.html}.stpl"
	done

########## Clean ##########

# Cleans all build artifacts
clean: clean-front _clean-rust

# Cleans astro-specific build artifacts like node_modules
clean-front: clean-astro
	rm -rf todo-front/astro/node_modules

# Cleans the built sailfish templates and public directory
clean-astro:
	rm -rf todo-front/templates
	rm -rf todo-front/public
	rm -rf todo-front/astro/dist

_clean-rust:
	cargo clean

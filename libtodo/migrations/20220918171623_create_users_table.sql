CREATE TABLE users(
    user_id        UUID         NOT NULL,
    username       VARCHAR(32)  NOT NULL UNIQUE,
    email          VARCHAR(255) NOT NULL UNIQUE,
    password_hash  TEXT         NOT NULL,
    created_at     TIMESTAMP    DEFAULT NOW(),
    PRIMARY KEY(user_id)
);

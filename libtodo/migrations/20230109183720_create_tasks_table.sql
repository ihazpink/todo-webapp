CREATE TABLE tasks(
    task_id    UUID        NOT NULL,
    text       TEXT        NOT NULL,
    status     VARCHAR(16) NOT NULL,
    created_at TIMESTAMP   DEFAULT NOW(),
    user_id    UUID        NOT NULL
        REFERENCES users (user_id),
    PRIMARY KEY(task_id)
);
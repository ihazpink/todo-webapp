BEGIN;
    UPDATE tasks
        SET title = 'No title'
        WHERE title IS NULL;
    ALTER TABLE tasks ALTER COLUMN title SET NOT NULL;
COMMIT;
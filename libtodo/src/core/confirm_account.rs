use crate::utils::error_chain_fmt;
use anyhow::Context;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(name = "Confirming a new user", skip(pool), err)]
pub async fn get(
    confirmation_token: String,
    pool: &PgPool,
) -> Result<(), ConfirmUserError> {
    let user_id = get_user_id_from_token(pool, &confirmation_token)
        .await
        .context("Failed to get the user id from the given token")?
        .ok_or(ConfirmUserError::UnknownToken)?;
    confirm_user(pool, user_id)
        .await
        .context("Failed to set the user status to `confirmed`")?;

    Ok(())
}

#[derive(thiserror::Error)]
pub enum ConfirmUserError {
    #[error("There is no user associated with the provided token")]
    UnknownToken,
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for ConfirmUserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

#[tracing::instrument(
    name = "Marking user as confirmed in db",
    skip(pool),
    err
)]
async fn confirm_user(pool: &PgPool, user_id: Uuid) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"UPDATE users SET status = 'confirmed' WHERE user_id = $1"#,
        user_id
    )
    .execute(pool)
    .await
    .map_err(|e| {
        tracing::error!("Failed to execute query: {e:?}");
        e
    })?;

    Ok(())
}

#[tracing::instrument(
    name = "Get user id from token",
    skip(confirmation_token, pool),
    err
)]
async fn get_user_id_from_token(
    pool: &PgPool,
    confirmation_token: &str,
) -> Result<Option<Uuid>, sqlx::Error> {
    let result = sqlx::query!(
        r#"SELECT user_id FROM user_tokens WHERE user_token = $1"#,
        confirmation_token,
    )
    .fetch_optional(pool)
    .await
    .map_err(|e| {
        tracing::error!("Failed to execute query: {e:?}");
        e
    })?;

    Ok(result.map(|r| r.user_id))
}

use crate::utils::generate_api_key;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(name = "Regenerating user's API key", skip(pool), err)]
pub async fn regenerate_api_key(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<(), anyhow::Error> {
    let new_api_key = generate_api_key();

    sqlx::query!(
        r#"
        UPDATE users
        SET api_key=$1
        WHERE user_id=$2
        "#,
        new_api_key,
        user_id,
    )
    .execute(pool)
    .await?;

    Ok(())
}

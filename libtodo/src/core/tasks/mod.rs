mod delete;
mod get;
mod patch;
mod post;

pub use delete::{delete, DeleteTaskError};
pub use get::{get, GetTasksError};
pub use patch::{patch, PatchTaskData, PatchTaskError};
pub use post::{post, PostTasksData, PostTasksError};

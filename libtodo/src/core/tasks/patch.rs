use crate::db;
use crate::utils::error_chain_fmt;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(serde::Deserialize, Debug)]
pub struct PatchTaskData {
    pub title: String,
    pub text: Option<String>,
    pub status: String,
}

#[tracing::instrument(name = "Updating a task", skip(pool), err)]
pub async fn patch(
    current_user_id: Uuid,
    task_data: PatchTaskData,
    task_id: Uuid,
    pool: &PgPool,
) -> Result<(), PatchTaskError> {
    let task = db::get_task_from_id(task_id, pool)
        .await
        .map_err(PatchTaskError::UnexpectedError)?;

    if task.user_id != current_user_id {
        return Err(PatchTaskError::Unauthorized);
    }

    sqlx::query!(
        r#"UPDATE tasks SET title=$1, text=$2, status=$3 WHERE task_id=$4"#,
        task_data.title,
        task_data.text,
        task_data.status,
        task_id
    )
    .execute(pool)
    .await
    .map_err(anyhow::Error::from)
    .map_err(PatchTaskError::UnexpectedError)?;

    Ok(())
}

#[derive(thiserror::Error)]
pub enum PatchTaskError {
    #[error("You are not authorized to edit this task")]
    Unauthorized,
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for PatchTaskError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

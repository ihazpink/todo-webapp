use crate::domain::Tag;
use crate::utils::error_chain_fmt;
use sqlx::PgPool;
use std::fmt::Debug;
use uuid::Uuid;

#[derive(serde::Deserialize, Debug)]
pub struct PostTasksData {
    pub title: String,
    pub text: Option<String>,
    /// Tags are encoded as a comma-seperated string
    pub tags: Option<String>,
}

#[tracing::instrument(name = "Creating a new task", skip(pool), err)]
pub async fn post(
    user_id: Uuid,
    task_data: PostTasksData,
    pool: &PgPool,
) -> Result<(), PostTasksError> {
    let text = task_data.text;
    let title = task_data.title;
    if title.trim().is_empty() {
        return Err(PostTasksError::ValidationError(anyhow::anyhow!(
            "Task title cannot be empty."
        )));
    }

    let tags = match task_data.tags {
        Some(t) => {
            Tag::parse_many(&t).map_err(PostTasksError::ValidationError)?
        }
        None => Vec::<Tag>::new(),
    };

    create_task(user_id, text, title, tags, pool)
        .await
        .map_err(PostTasksError::UnexpectedError)?;

    Ok(())
}

#[derive(thiserror::Error)]
#[error(transparent)]
pub enum PostTasksError {
    #[error("{0}")]
    ValidationError(anyhow::Error),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl Debug for PostTasksError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

#[tracing::instrument(
    name = "Adding a new task to the database",
    fields(user_id, text, title, tags, task_id)
    skip(pool),
    err
)]
async fn create_task(
    user_id: Uuid,
    text: Option<String>,
    title: String,
    tags: Vec<Tag>,
    pool: &PgPool,
) -> Result<Uuid, anyhow::Error> {
    let task_id = Uuid::new_v4();
    tracing::Span::current().record("task_id", task_id.to_string());

    let tags = tags
        .into_iter()
        .map(Tag::into_inner)
        .collect::<Vec<String>>();

    sqlx::query!(
        r#"
        INSERT INTO tasks (task_id, text, status, user_id, title, tags)
        VALUES ($1, $2, $3, $4, $5, $6)
        "#,
        task_id,
        text,
        "incomplete",
        user_id,
        title,
        &tags,
    )
    .execute(pool)
    .await?;

    Ok(task_id)
}

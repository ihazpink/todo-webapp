use crate::auth::compute_password_hash;
use crate::db::create_or_update_confirmation_token;
use crate::domain::{Email, NewUser, Username};
use crate::email_client::EmailClient;
use crate::telemetry::spawn_blocking_with_tracing;
use crate::utils::{
    error_chain_fmt, generate_api_key, generate_confirmation_token,
};
use anyhow::Context;
use secrecy::{ExposeSecret, Secret};
use sqlx::{PgPool, Postgres, Transaction};
use std::convert::{TryFrom, TryInto};
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct UserData {
    pub username: String,
    pub email: String,
    pub password: Secret<String>,
}

impl TryFrom<UserData> for NewUser {
    type Error = anyhow::Error;
    fn try_from(value: UserData) -> Result<Self, Self::Error> {
        let username = Username::parse(value.username)?;
        let email = Email::parse(value.email)?;

        Ok(NewUser {
            username,
            email,
            password: value.password,
        })
    }
}

#[tracing::instrument(
    name = "Adding a new user",
    skip(user_data, pool, email_client, base_url),
    fields(
        username = %user_data.username,
        email = %user_data.email
    ),
    err
)]
pub async fn post<P: AsRef<str>>(
    user_data: UserData,
    pool: &PgPool,
    email_client: &EmailClient,
    base_url: P,
) -> Result<(), PostUsersError> {
    let new_user = user_data
        .try_into()
        .map_err(PostUsersError::ValidationError)?;
    let mut transaction = pool
        .begin()
        .await
        .context("Failed to acquire a Postgres connection from the pool")?;

    let user_id = insert_user(&mut transaction, &new_user)
        .await
        .context("Failed to insert new user in the database.")?;

    let confirmation_token = generate_confirmation_token();
    create_or_update_confirmation_token(&mut transaction, user_id, &confirmation_token)
        .await
        .context("Failed to store the confirmation token for a new user.")?;

    transaction
        .commit()
        .await
        .context("Failed to commit the SQL transaction to store a new user.")?;

    send_confirmation_email(
        email_client,
        new_user.email,
        base_url.as_ref(),
        &confirmation_token,
    )
    .await
    .map_err(|_| PostUsersError::EmailError)?;

    Ok(())
}
#[derive(thiserror::Error)]
pub enum PostUsersError {
    #[error("{0}")]
    ValidationError(anyhow::Error),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
    #[error("Failed to send confirmation email.")]
    EmailError,
}

impl std::fmt::Debug for PostUsersError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

#[tracing::instrument(
    name = "Send a confirmation email to a new user",
    skip(email_client, confirmation_token),
    err
)]
pub async fn send_confirmation_email(
    email_client: &EmailClient,
    user_email: Email,
    base_url: &str,
    confirmation_token: &str,
) -> Result<(), reqwest::Error> {
    let confirmation_link = format!(
        "{}/confirm-account?confirmation_token={}",
        base_url, confirmation_token,
    );
    let plain_body = format!(
        "Welcome to our todo website!\nVisit {} to confirm your account.",
        confirmation_link
    );
    let html_body = format!(
        "Welcome to our todo website!<br /> \
                Click <a href=\"{}\">here</a> to confirm your account.",
        confirmation_link,
    );

    email_client
        .send_email(user_email, "Welcome!", &html_body, &plain_body)
        .await
}

#[tracing::instrument(
    name = "Insert user into the database",
    skip(transaction, user),
    err
)]
async fn insert_user(
    transaction: &mut Transaction<'_, Postgres>,
    user: &NewUser,
) -> Result<Uuid, anyhow::Error> {
    let user_id = Uuid::new_v4();
    let password = user.password.clone();
    let password_hash =
        spawn_blocking_with_tracing(move || compute_password_hash(password))
            .await?
            .context("Failed to hash password")?;
    let api_key = generate_api_key();

    sqlx::query!(
        r#"
        INSERT INTO users (user_id, email, username, password_hash, status, api_key)
        VALUES ($1, $2, $3, $4, $5, $6)
        "#,
        user_id,
        user.email.as_ref(),
        user.username.as_ref(),
        password_hash.expose_secret(),
        String::from("pending_confirmation"),
        api_key
    )
    .execute(transaction)
    .await?;

    Ok(user_id)
}

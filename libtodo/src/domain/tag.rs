#[derive(Debug)]
pub struct Tag(String);

impl Tag {
    pub fn parse(s: &str) -> Result<Tag, anyhow::Error> {
        let s = s.trim();

        if s.is_empty() {
            anyhow::bail!("Tag is empty");
        }

        if !s.chars().all(|c| c.is_alphanumeric() || c.is_whitespace()) {
            anyhow::bail!("Tag contains non-alphanumeric characters")
        }

        if s.chars().count() > 32 {
            anyhow::bail!("Tag is too long");
        }

        Ok(Tag(s.to_string()))
    }

    pub fn parse_many(s: &str) -> Result<Vec<Tag>, anyhow::Error> {
        s.split(',')
            .map(Tag::parse)
            .collect::<Result<Vec<Tag>, anyhow::Error>>()
    }

    pub fn into_inner(self) -> String {
        self.0
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::Tag;
    use claim::{assert_err, assert_ok};

    #[test]
    fn a_32_char_tag_is_valid() {
        let tag = "a".repeat(32);
        assert_ok!(Tag::parse(&tag));
    }

    #[test]
    fn a_33_char_long_tag_is_rejected() {
        let tag = "a".repeat(33);
        assert_err!(Tag::parse(&tag));
    }

    #[test]
    fn whitespace_only_tags_are_rejected() {
        let tag = " ";
        assert_err!(Tag::parse(tag));
    }

    #[test]
    fn empty_string_tag_is_rejected() {
        let tag = "";
        assert_err!(Tag::parse(tag));
    }

    #[test]
    fn tags_containing_non_alphanumeric_chars_are_rejected() {
        let tag = "/😀";
        assert_err!(Tag::parse(tag));
    }

    #[test]
    fn a_valid_tag_is_parsed_successfully() {
        let tag = "Work2";
        assert_ok!(Tag::parse(tag));
    }

    #[test]
    fn leading_and_trailing_whitespace_are_stripped() {
        let tag = "     work     ";
        let tag = Tag::parse(tag).unwrap();
        assert_eq!(tag.0, "work");
    }

    #[test]
    fn case_is_preserved() {
        let tag = Tag::parse("CaSEiSpreSErvEd").unwrap();
        assert_eq!(tag.0, "CaSEiSpreSErvEd");
    }

    #[test]
    fn whitespace_between_words_is_alowed() {
        let tag = Tag::parse("For work").unwrap();
        assert_eq!(tag.0, "For work");
    }
}

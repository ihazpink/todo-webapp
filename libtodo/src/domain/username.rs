#[derive(Debug, serde::Serialize, serde::Deserialize, Clone)]
pub struct Username(String);

impl Username {
    pub fn parse(s: String) -> Result<Username, anyhow::Error> {
        if s.trim().is_empty() {
            anyhow::bail!("Username is emtpy.");
        }

        if !s.chars().all(char::is_alphanumeric) {
            anyhow::bail!("Username contains non-alphanumeric characters.");
        }

        if s.chars().count() > 32 {
            anyhow::bail!("Username is too long.");
        }

        Ok(Username(s))
    }
}

impl AsRef<str> for Username {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::Username;
    use claim::{assert_err, assert_ok};

    #[test]
    fn a_32_char_long_name_is_valid() {
        let name = "a".repeat(32);
        assert_ok!(Username::parse(name));
    }

    #[test]
    fn a_33_char_long_name_is_rejected() {
        let name = "a".repeat(33);
        assert_err!(Username::parse(name));
    }

    #[test]
    fn whitespace_only_names_are_rejected() {
        let name = " ".to_string();
        assert_err!(Username::parse(name));
    }

    #[test]
    fn empty_string_is_rejected() {
        let name = "".to_string();
        assert_err!(Username::parse(name));
    }

    #[test]
    fn names_containing_non_alphanumeric_chars_are_rejected() {
        let name = "/😀".to_string();
        assert_err!(Username::parse(name));
    }

    #[test]
    fn a_valid_username_is_parsed_successfully() {
        let name = "mycoolusername1234".to_string();
        assert_ok!(Username::parse(name));
    }
}

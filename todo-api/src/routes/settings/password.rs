use crate::auth::UserId;
use actix_web::{web, HttpResponse};
use libtodo::core;
use libtodo::core::settings::password::{
    PatchPasswordData, PatchPasswordError,
};
use sqlx::PgPool;

pub async fn patch(
    body: web::Json<PatchPasswordData>,
    pool: web::Data<PgPool>,
    user_id: web::ReqData<UserId>,
) -> HttpResponse {
    let user_id = user_id.into_inner();
    match core::settings::password::patch(body.0, &pool, *user_id).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(PatchPasswordError::AuthError(_)) => {
            HttpResponse::Unauthorized().finish()
        }
        Err(PatchPasswordError::ValidationError(_)) => {
            HttpResponse::BadRequest().finish()
        }
        Err(PatchPasswordError::UnexpectedError(_)) => {
            HttpResponse::InternalServerError().finish()
        }
    }
}

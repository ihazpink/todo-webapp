use crate::helpers::spawn_app;
use uuid::Uuid;

#[tokio::test]
async fn non_existing_user_is_rejected() {
    // Arrange
    let app = spawn_app().await;
    // Random credentials
    let body = serde_json::json!({
        "username": Uuid::new_v4().to_string(),
        "password": Uuid::new_v4().to_string()
    });

    // Act
    let response = app.post_api_session(&body).await;

    // Assert
    assert_eq!(401, response.status().as_u16());
}

#[tokio::test]
async fn invalid_password_is_rejected() {
    // Arrange
    let app = spawn_app().await;

    let password = Uuid::new_v4().to_string();
    assert_ne!(app.test_user.password, password);

    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": password
    });

    // Act
    let response = app.post_api_session(&body).await;

    // Assert
    assert_eq!(401, response.status().as_u16());
}

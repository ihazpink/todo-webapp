use crate::helpers::spawn_app;

#[tokio::test]
async fn post_tasks_returns_401_for_requests_missing_auth() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let body = serde_json::json!({
        "text": "Yaahoo",
        "title": "Task title",
        "tags": "foo, bar",
    });
    let response = app.post_tasks(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 401);
}

#[tokio::test]
async fn post_tasks_returns_400_for_requests_without_task_text() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - create a session
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_api_session(&body).await;

    // Act part 2
    let body = serde_json::json!({}); // Empty body
    let response = app.post_tasks(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn tasks_without_title_are_rejected() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - create a session
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_api_session(&body).await;

    // Act part 2
    let body = serde_json::json!({ "text": "Yaahoo" });
    let response = app.post_tasks(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn only_title_is_mandatory() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - create a session
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_api_session(&body).await;

    // Act part 2
    let body = serde_json::json!({ "title": "My tag title" });
    let response = app.post_tasks(&body).await;

    // Assert
    let saved = sqlx::query!(
        r#"SELECT text, status, title, tags FROM tasks WHERE user_id=$1"#,
        &app.test_user.user_id,
    )
    .fetch_one(&app.pool)
    .await
    .expect("Failed to fetch saved task");

    assert_eq!(response.status().as_u16(), 200);
    assert_eq!(saved.title, "My tag title");
    assert_eq!(saved.text, None);
    assert_eq!(saved.tags, Vec::<String>::new());
    assert_eq!(saved.status, "uncompleted");
}

#[tokio::test]
async fn requests_with_all_fields_set_work_correctly() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - create a session
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_api_session(&body).await;

    // Act part 2
    let body = serde_json::json!({
        "title": "My Awesome Title",
        "tags": "work, foo bar",
        "text": "Yaahoo"
    });
    let response = app.post_tasks(&body).await;

    // Assert
    let saved = sqlx::query!(
        r#"SELECT text, status, title, tags FROM tasks WHERE user_id=$1"#,
        &app.test_user.user_id,
    )
    .fetch_one(&app.pool)
    .await
    .expect("Failed to fetch saved task");

    assert_eq!(response.status().as_u16(), 200);
    assert_eq!(saved.status, "uncompleted");
    assert_eq!(saved.text, Some("Yaahoo".to_string()));
    assert_eq!(saved.title, "My Awesome Title");
    assert_eq!(saved.tags, vec!["work", "foo bar"]);
}

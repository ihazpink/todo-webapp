use actix_session::SessionInsertError;
use actix_web::http::header::{ContentType, LOCATION};
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::{FlashMessage, IncomingFlashMessages};
use libtodo::auth::{validate_credentials, AuthError, Credentials};
use libtodo::domain::Username;
use libtodo::utils::error_chain_fmt;
use sailfish::TemplateOnce;
use sqlx::PgPool;

#[derive(sailfish::TemplateOnce)]
#[template(path = "login.stpl")]
struct LoginTemplate {
    pub flash_messages: IncomingFlashMessages,
}

pub async fn get(flash_messages: IncomingFlashMessages) -> HttpResponse {
    let body = LoginTemplate { flash_messages }.render_once().unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body)
}

pub async fn post(
    credentials: web::Form<Credentials>,
    pool: web::Data<PgPool>,
    session: crate::session::TypedSession,
) -> Result<HttpResponse, CreateSessionError> {
    tracing::Span::current()
        .record("username", &tracing::field::display(&credentials.username));

    let username = Username::parse(credentials.username.clone())?;
    let user_id = validate_credentials(credentials.0, &pool).await?;
    session.renew();
    session.insert_user(user_id, username)?;

    tracing::Span::current()
        .record("user_id", &tracing::field::display(&user_id));

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/tasks"))
        .finish())
}

#[derive(thiserror::Error)]
pub enum CreateSessionError {
    #[error("Authentication failed")]
    AuthError(#[source] anyhow::Error),
    #[error("Something went wrong")]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for CreateSessionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl From<AuthError> for CreateSessionError {
    fn from(value: AuthError) -> Self {
        match value {
            AuthError::InvalidCredentials(e) => {
                CreateSessionError::AuthError(e)
            }
            AuthError::UnexpectedError(e) => {
                CreateSessionError::UnexpectedError(e)
            }
        }
    }
}

impl From<SessionInsertError> for CreateSessionError {
    fn from(value: SessionInsertError) -> Self {
        CreateSessionError::UnexpectedError(value.into())
    }
}

impl ResponseError for CreateSessionError {
    fn error_response(&self) -> HttpResponse {
        FlashMessage::error(self.to_string()).send();
        HttpResponse::SeeOther()
            .insert_header((LOCATION, "/login"))
            .finish()
    }
}

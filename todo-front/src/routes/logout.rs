use crate::session::TypedSession;
use actix_web::http::header::LOCATION;
use actix_web::HttpResponse;

#[tracing::instrument(name = "Logging user out", skip(session))]
pub async fn post(session: TypedSession) -> HttpResponse {
    session.purge();
    HttpResponse::SeeOther()
        .insert_header((LOCATION, "/"))
        .finish()
}

use crate::auth::UserId;
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::FlashMessage;
use libtodo::core::settings::regenerate_api_key;
use libtodo::utils::error_chain_fmt;
use reqwest::header::LOCATION;
use sqlx::PgPool;
use std::fmt::Debug;

pub async fn post(
    user_id: web::ReqData<UserId>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, ResetApiKeyError> {
    let user_id = user_id.into_inner();

    regenerate_api_key(*user_id, &pool).await?;

    FlashMessage::success("API key regenerated successfully.").send();
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/settings"))
        .finish())
}

#[derive(thiserror::Error)]
#[error(transparent)]
pub struct ResetApiKeyError(#[from] anyhow::Error);

impl Debug for ResetApiKeyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for ResetApiKeyError {
    fn error_response(&self) -> HttpResponse {
        FlashMessage::error("Failed to regenerate API key.").send();
        HttpResponse::SeeOther()
            .insert_header((LOCATION, "/settings"))
            .finish()
    }
}

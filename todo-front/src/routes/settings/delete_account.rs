use crate::auth::UserId;
use crate::session::TypedSession;
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::FlashMessage;
use libtodo::auth::{validate_credentials, AuthError, Credentials};
use libtodo::db;
use libtodo::utils::error_chain_fmt;
use reqwest::header::LOCATION;
use sqlx::PgPool;
use std::fmt::Debug;

pub async fn post(
    user_id: web::ReqData<UserId>,
    credentials: web::Form<Credentials>,
    pool: web::Data<PgPool>,
    session: TypedSession,
) -> Result<HttpResponse, DeleteAccountError> {
    tracing::Span::current()
        .record("username", &tracing::field::display(&credentials.username));

    let user_id = user_id.into_inner();
    let stored_user_id = validate_credentials(credentials.0, &pool).await?;

    if stored_user_id != *user_id {
        return Err(AuthError::InvalidCredentials(anyhow::anyhow!(
            "User submitted credentials for wrong user"
        ))
        .into());
    }

    db::delete_user(*user_id, &pool).await?;

    session.purge();
    FlashMessage::success("Account deleted successfully.").send();
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/"))
        .finish())
}

#[derive(thiserror::Error)]
pub enum DeleteAccountError {
    #[error("Failed to confirm credentials.")]
    AuthError(#[from] AuthError),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl Debug for DeleteAccountError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for DeleteAccountError {
    fn error_response(&self) -> HttpResponse {
        FlashMessage::error(self.to_string()).send();
        HttpResponse::SeeOther()
            .insert_header((LOCATION, "/settings"))
            .finish()
    }
}

use crate::auth::UserId;
use crate::utils::ApplicationBaseUrl;
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::FlashMessage;
use libtodo::core::settings::{change_email, ChangeEmailData, ChangeEmailError};
use libtodo::email_client::EmailClient;
use reqwest::header::LOCATION;
use sqlx::PgPool;

pub async fn post(
    user_id: web::ReqData<UserId>,
    email_data: web::Form<ChangeEmailData>,
    pool: web::Data<PgPool>,
    email_client: web::Data<EmailClient>,
    base_url: web::Data<ApplicationBaseUrl>,
) -> HttpResponse {
    let user_id = user_id.into_inner();

    match change_email(
        email_data.0,
        *user_id,
        &pool,
        &email_client,
        &base_url.0,
    )
    .await
    {
        Ok(_) => {
            FlashMessage::success("Email updated successfully. Confirmation email sent.")
            .send();
        }
        Err(ChangeEmailError::SendEmailError) => {
            FlashMessage::success("Email updated successfully.").send();
            FlashMessage::warning(ChangeEmailError::SendEmailError.to_string()).send();
        }
        Err(e) => {
            FlashMessage::error(e.to_string()).send();
        }
    }

    HttpResponse::SeeOther()
        .insert_header((LOCATION, "/settings"))
        .finish()
}

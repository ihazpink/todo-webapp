use crate::auth::UserId;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::IncomingFlashMessages;
use libtodo::db;
use libtodo::domain::{User, Username};
use libtodo::utils::error_chain_fmt;
use sailfish::TemplateOnce;
use sqlx::PgPool;
use std::fmt::Debug;

#[derive(sailfish::TemplateOnce)]
#[template(path = "settings.stpl")]
struct SettingsTemplate {
    pub flash_messages: IncomingFlashMessages,
    pub user: User,
    pub username: Option<Username>,
}

pub async fn get(
    user_id: web::ReqData<UserId>,
    flash_messages: IncomingFlashMessages,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, GetSettingsError> {
    let user = db::get_user_from_id(**user_id, &pool).await?;
    let username = user.username.clone();

    let body = SettingsTemplate {
        flash_messages,
        user,
        username: Some(username),
    }
    .render_once()
    .unwrap();

    Ok(HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body))
}

#[derive(thiserror::Error)]
#[error(transparent)]
pub struct GetSettingsError(#[from] anyhow::Error);

impl Debug for GetSettingsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for GetSettingsError {
    fn error_response(&self) -> HttpResponse {
        todo!()
    }
}

use crate::utils::ApplicationBaseUrl;
use actix_web::http::header::{ContentType, LOCATION};
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::{FlashMessage, IncomingFlashMessages};
use libtodo::core;
use libtodo::core::users::{PostUsersError, UserData};
use libtodo::email_client::EmailClient;
use sailfish::TemplateOnce;
use secrecy::{ExposeSecret, Secret};
use sqlx::PgPool;

#[derive(sailfish::TemplateOnce)]
#[template(path = "signup.stpl")]
struct SignupTemplate {
    pub flash_messages: IncomingFlashMessages,
}

pub async fn get(flash_messages: IncomingFlashMessages) -> HttpResponse {
    let body = SignupTemplate { flash_messages }.render_once().unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body)
}

#[derive(serde::Deserialize)]
pub struct SignupFormData {
    pub username: String,
    pub email: String,
    pub password: Secret<String>,
    pub password_check: Secret<String>,
}

impl From<SignupFormData> for UserData {
    fn from(value: SignupFormData) -> Self {
        UserData {
            username: value.username,
            email: value.email,
            password: value.password,
        }
    }
}

pub async fn post(
    form: web::Form<SignupFormData>,
    pool: web::Data<PgPool>,
    email_client: web::Data<EmailClient>,
    base_url: web::Data<ApplicationBaseUrl>,
) -> HttpResponse {
    if form.password.expose_secret() != form.password_check.expose_secret() {
        FlashMessage::error("You entered two different passwords - the field values must match.").send();
        return HttpResponse::SeeOther()
            .insert_header((LOCATION, "/signup"))
            .finish();
    }

    let user_data = form.0.into();
    let resp =
        core::users::post(user_data, &pool, &email_client, base_url.0.as_str())
            .await;
    match resp {
        Ok(_) => HttpResponse::SeeOther()
            .insert_header((LOCATION, "/tasks"))
            .finish(),
        Err(e) => {
            match e {
                PostUsersError::UnexpectedError(_) => {
                    FlashMessage::error("A fatal error occured on our end")
                        .send();
                }
                PostUsersError::ValidationError(e) => {
                    FlashMessage::error(e.to_string()).send()
                }
                PostUsersError::EmailError => {
                    FlashMessage::warning(PostUsersError::EmailError.to_string())
                        .send();
                    FlashMessage::warning("You can request that a new confirmation email is sent from the settings.")
                        .send();
                }
            }
            HttpResponse::SeeOther()
                .insert_header((LOCATION, "/signup"))
                .finish()
        }
    }
}

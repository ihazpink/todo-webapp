use crate::auth::UserId;
use crate::session::TypedSession;
use actix_web::http::header::{ContentType, LOCATION};
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::{FlashMessage, IncomingFlashMessages};
use libtodo::core;
use libtodo::core::tasks::{PatchTaskData, PatchTaskError};
use libtodo::domain::{Task, Username};
use sailfish::TemplateOnce;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(sailfish::TemplateOnce)]
#[template(path = "task-edit.stpl")]
pub struct EditTaskTemplate {
    pub task: Task,
    pub flash_messages: IncomingFlashMessages,
    pub username: Option<Username>,
}

#[tracing::instrument(
    name = "Getting a task's edit page",
    skip(flash_messages, pool, session)
)]
pub async fn get(
    user_id: web::ReqData<UserId>,
    task_id: web::Path<Uuid>,
    flash_messages: IncomingFlashMessages,
    pool: web::Data<PgPool>,
    session: TypedSession,
) -> HttpResponse {
    let task = match libtodo::db::get_task_from_id(*task_id, &pool).await {
        Ok(t) => t,
        Err(e) => {
            FlashMessage::error(e.to_string()).send();
            return HttpResponse::SeeOther()
                .insert_header((LOCATION, format!("/tasks/{task_id:}/edit")))
                .finish();
        }
    };

    let username = session.get_username().unwrap_or(None);
    let body = EditTaskTemplate {
        task,
        flash_messages,
        username,
    }
    .render_once()
    .unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body)
}

#[derive(serde::Deserialize, Debug)]
pub struct EditTaskFormData {
    pub title: String,
    pub text: Option<String>,
    pub tags: Option<String>,
    pub completed: Option<bool>,
}

impl From<EditTaskFormData> for PatchTaskData {
    fn from(value: EditTaskFormData) -> Self {
        let status = match value.completed {
            Some(true) => "complete",
            _ => "incomplete",
        }
        .to_string();

        PatchTaskData {
            title: value.title,
            text: value.text,
            status,
        }
    }
}

#[tracing::instrument(name = "Editing a task", skip(pool))]
pub async fn post(
    user_id: web::ReqData<UserId>,
    task_id: web::Path<Uuid>,
    pool: web::Data<PgPool>,
    task_data: web::Form<EditTaskFormData>,
) -> HttpResponse {
    let user_id = user_id.into_inner();
    let task_data = task_data.0.into();

    match core::tasks::patch(*user_id, task_data, *task_id, &pool).await {
        Ok(_) => HttpResponse::SeeOther()
            .insert_header((LOCATION, "/tasks"))
            .finish(),
        Err(PatchTaskError::Unauthorized) => {
            FlashMessage::error(PatchTaskError::Unauthorized.to_string())
                .send();
            HttpResponse::SeeOther()
                .insert_header((LOCATION, "/tasks"))
                .finish()
        }
        Err(e) => {
            FlashMessage::error(e.to_string()).send();
            HttpResponse::SeeOther()
                .insert_header((LOCATION, format!("/tasks/{task_id:}/edit")))
                .finish()
        }
    }
}

use crate::auth::UserId;
use crate::session::TypedSession;
use actix_web::http::header::ContentType;
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::IncomingFlashMessages;
use libtodo::core;
use libtodo::domain::{Task, Username};
use sailfish::TemplateOnce;
use sqlx::PgPool;

#[derive(sailfish::TemplateOnce)]
#[template(path = "tasks.stpl")]
pub struct TasksTemplate {
    pub tasks: Vec<Task>,
    pub flash_messages: IncomingFlashMessages,
    pub username: Option<Username>,
}

pub async fn get(
    user_id: web::ReqData<UserId>,
    pool: web::Data<PgPool>,
    flash_messages: IncomingFlashMessages,
    session: TypedSession,
) -> HttpResponse {
    let username = session.get_username().unwrap_or(None);
    let user_id = user_id.into_inner();
    let tasks = core::tasks::get(*user_id, &pool).await.unwrap();

    let body = TasksTemplate {
        tasks,
        flash_messages,
        username,
    }
    .render_once()
    .unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body)
}

mod delete;
pub mod edit;
mod get;
mod post;
mod toggle_status;

pub use delete::delete;
pub use get::{get, TasksTemplate};
pub use post::post;
pub use toggle_status::{toggle_status, ToggleStatusError};

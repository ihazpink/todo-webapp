use crate::auth::UserId;
use actix_web::http::header::LOCATION;
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::FlashMessage;
use libtodo::core::tasks::{PatchTaskData, PatchTaskError};
use libtodo::utils::error_chain_fmt;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(name = "Changing a task's status", skip(pool), err)]
pub async fn toggle_status(
    user_id: web::ReqData<UserId>,
    task_id: web::Path<Uuid>,
    pool: web::Data<PgPool>,
) -> Result<HttpResponse, ToggleStatusError> {
    let user_id = user_id.into_inner();
    let task = libtodo::db::get_task_from_id(*task_id, &pool).await?;

    let task_data = match task.status.as_str() {
        "complete" => PatchTaskData {
            title: task.title,
            text: task.text,
            status: "incomplete".to_string(),
        },
        _ => PatchTaskData {
            title: task.title,
            text: task.text,
            status: "complete".to_string(),
        },
    };

    libtodo::core::tasks::patch(*user_id, task_data, *task_id, &pool).await?;

    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/tasks"))
        .finish())
}

#[derive(thiserror::Error)]
pub enum ToggleStatusError {
    #[error("You are not authorized to modify this task")]
    Unauthorized,
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for ToggleStatusError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(&self, f)
    }
}

impl ResponseError for ToggleStatusError {
    fn error_response(&self) -> HttpResponse {
        FlashMessage::error(self.to_string()).send();
        HttpResponse::SeeOther()
            .insert_header((LOCATION, "/tasks"))
            .finish()
    }
}

impl From<PatchTaskError> for ToggleStatusError {
    fn from(value: PatchTaskError) -> Self {
        match value {
            PatchTaskError::Unauthorized => ToggleStatusError::Unauthorized,
            PatchTaskError::UnexpectedError(e) => {
                ToggleStatusError::UnexpectedError(e)
            }
        }
    }
}

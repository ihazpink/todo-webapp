use crate::auth::redirect_anonymous_users_to_login;
use crate::config::{Config, PostgresConfig};
use crate::routes;
use crate::utils::ApplicationBaseUrl;
use actix_files::Files;
use actix_session::storage::RedisSessionStore;
use actix_session::SessionMiddleware;
use actix_web::cookie::Key;
use actix_web::dev::Server;
use actix_web::{web, App, HttpServer};
use actix_web_flash_messages::storage::CookieMessageStore;
use actix_web_flash_messages::FlashMessagesFramework;
use actix_web_lab::middleware::from_fn;
use libtodo::email_client::EmailClient;
use secrecy::{ExposeSecret, Secret};
use sqlx::postgres::PgPoolOptions;
use sqlx::PgPool;
use std::net::TcpListener;
use tracing_actix_web::TracingLogger;

pub struct Application {
    pub port: u16,
    server: Server,
}

impl Application {
    pub async fn build(config: Config) -> Result<Self, anyhow::Error> {
        let pool = get_pool(&config.postgres);

        let sender_email = config
            .email_client
            .sender()
            .expect("Invalid sender email address.");
        let timeout = config.email_client.timeout();
        let email_client = EmailClient::new(
            config.email_client.base_url.clone(),
            sender_email,
            config.email_client.auth_token.clone(),
            timeout,
        );

        let address = format!("{}:{}", config.host, config.port);
        let listener = TcpListener::bind(address)?;
        let port = listener.local_addr().unwrap().port();
        let server = run(
            listener,
            pool,
            email_client,
            config.base_url,
            config.hmac_secret,
            config.redis_uri,
        )
        .await?;

        Ok(Self { port, server })
    }

    pub async fn run_until_stopped(self) -> Result<(), std::io::Error> {
        self.server.await
    }
}

pub fn get_pool(config: &PostgresConfig) -> PgPool {
    PgPoolOptions::new()
        .acquire_timeout(std::time::Duration::from_secs(2))
        .connect_lazy_with(config.with_db())
}

async fn run(
    listener: TcpListener,
    pool: PgPool,
    email_client: EmailClient,
    base_url: String,
    hmac_secret: Secret<String>,
    redis_uri: Secret<String>,
) -> Result<Server, anyhow::Error> {
    let pool = web::Data::new(pool);
    let email_client = web::Data::new(email_client);
    let base_url = web::Data::new(ApplicationBaseUrl(base_url));
    let secret_key = Key::from(hmac_secret.expose_secret().as_bytes());
    let redis_store = RedisSessionStore::new(redis_uri.expose_secret()).await?;
    let message_store = CookieMessageStore::builder(Key::from(
        hmac_secret.expose_secret().as_bytes(),
    ))
    .build();
    let message_framework =
        FlashMessagesFramework::builder(message_store).build();

    libtodo::run_migrations(&pool).await;

    let server = HttpServer::new(move || {
        App::new()
            .wrap(TracingLogger::default())
            .wrap(SessionMiddleware::new(
                redis_store.clone(),
                secret_key.clone(),
            ))
            .wrap(message_framework.clone())
            .route("/", web::get().to(routes::index::get))
            .route("/login", web::get().to(routes::login::get))
            .route("/login", web::post().to(routes::login::post))
            .route("/signup", web::get().to(routes::signup::get))
            .route("/signup", web::post().to(routes::signup::post))
            .route("/logout", web::post().to(routes::logout::post))
            .service(
                web::scope("/tasks")
                    .wrap(from_fn(redirect_anonymous_users_to_login))
                    .route("", web::get().to(routes::tasks::get))
                    .route("", web::post().to(routes::tasks::post))
                    .route(
                        "/{task_id}/delete",
                        web::post().to(routes::tasks::delete),
                    )
                    .route(
                        "/{task_id}/toggle-status",
                        web::post().to(routes::tasks::toggle_status),
                    )
                    .route(
                        "/{task_id}/edit",
                        web::get().to(routes::tasks::edit::get),
                    )
                    .route(
                        "/{task_id}/edit",
                        web::post().to(routes::tasks::edit::post),
                    ),
            )
            .service(
                web::scope("/settings")
                    .wrap(from_fn(redirect_anonymous_users_to_login))
                    .route("", web::get().to(routes::settings::get))
                    .route(
                        "/api-key",
                        web::post().to(routes::settings::api_key::post),
                    )
                    .route(
                        "/resend-confirmation-email",
                        web::post().to(
                            routes::settings::resend_confirmation_email::post,
                        ),
                    )
                    .route(
                        "/delete-account",
                        web::post().to(routes::settings::delete_account::post),
                    )
                    .route(
                        "/email",
                        web::post().to(routes::settings::email::post),
                    )
                    .route(
                        "/password",
                        web::post().to(routes::settings::password::post)
                    )
            )
            .service(Files::new("/", "public/").prefer_utf8(true))
            .app_data(pool.clone())
            .app_data(email_client.clone())
            .app_data(base_url.clone())
            .app_data(web::Data::new(HmacSecret(hmac_secret.clone())))
    })
    .listen(listener)?
    .run();
    Ok(server)
}

#[derive(Clone)]
pub struct HmacSecret(pub Secret<String>);

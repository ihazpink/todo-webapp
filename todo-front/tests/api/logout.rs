use crate::helpers::{assert_is_redirect_to, spawn_app};

#[tokio::test]
// We always redirect to index because if the user sends a invalid session
// user_id that means they weren't logged in anyway.
// In this case we're sending an empty user_id which should always be invalid.
async fn redirects_to_index_when_invalid_session_id_is_given() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.logout().await;

    // Assert
    assert_is_redirect_to(&response, "/");
}

#[tokio::test]
async fn redirects_to_index_after_logging_user_out() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": app.test_user.username,
        "password": app.test_user.password
    });

    app.post_login(&body).await;

    // Act
    let response = app.logout().await;

    // Assert
    assert_is_redirect_to(&response, "/");
}

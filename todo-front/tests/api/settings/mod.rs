use crate::helpers::{assert_is_redirect_to, spawn_app};

mod api_key;
mod delete_account;
mod email;
mod password;
mod resend_confirmation_email;

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app
        .api_client
        .get(format!("{}/settings", &app.address))
        .send()
        .await
        .expect("Failed to execute request");

    // Assert
    assert_is_redirect_to(&response, "/login");
}

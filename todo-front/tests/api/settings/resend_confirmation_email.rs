use crate::helpers::{assert_is_redirect_to, spawn_app};
use serde_json::json;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.resend_confirmation_email().await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn sets_flash_message_upon_success() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    let response = app.resend_confirmation_email().await;

    // Assert
    assert_is_redirect_to(&response, "/settings");
    let settings_html = app.get_settings_html().await;
    assert!(settings_html.contains("<span>Sent new confirmation email.</span>"));
}

#[tokio::test]
async fn sends_new_confirmation_email() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.resend_confirmation_email().await;

    // Assert
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    assert_eq!(confirmation_links.html, confirmation_links.plain_text);
}

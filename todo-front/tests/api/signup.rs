use crate::helpers::{assert_is_redirect_to, spawn_app};
use argon2::{Argon2, PasswordHash, PasswordVerifier};
use uuid::Uuid;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn redirects_to_tasks_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act
    let response = app.post_signup(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/tasks");
}

#[tokio::test]
async fn persists_the_new_user() {
    // Arrange
    let app = spawn_app().await;
    let username = String::from("testusername");
    let body = serde_json::json!({
        "username": &username,
        "password": "mysupersecretpassword123+-",
        "password_check": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    // Act
    app.post_signup(&body).await;

    // Assert
    let saved = sqlx::query!(
        "SELECT email, username, status FROM users WHERE username=$1",
        username
    )
    .fetch_one(&app.pool)
    .await
    .expect("Failed to fetch users.");

    assert_eq!(saved.email, "testuser@example.com");
    assert_eq!(saved.username, "testusername");
    assert_eq!(saved.status, "pending_confirmation");
}

#[tokio::test]
async fn sends_a_confirmation_email_for_valid_data() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act
    app.post_signup(&body).await;

    // Assert
    // Mock asserts on drop
}

#[tokio::test]
async fn sends_a_confirmation_email_with_a_link() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.post_signup(&body).await;

    // Assert
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    assert_eq!(confirmation_links.html, confirmation_links.plain_text);
}

#[tokio::test]
async fn displays_flash_message_upon_fatal_db_failure() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });
    // Sabotage the database
    sqlx::query!("ALTER TABLE users DROP COLUMN email;")
        .execute(&app.pool)
        .await
        .unwrap();

    // Act
    let response = app.post_signup(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/signup");

    let html_page = app.get_signup_html().await;
    assert!(
        html_page.contains(r#"<span>A fatal error occured on our end</span>"#)
    );
}

#[tokio::test]
async fn stores_hashed_user_password() {
    // Arrange
    let app = spawn_app().await;
    let username = String::from("mysupercoolusername");
    let password = Uuid::new_v4().to_string();
    let body = serde_json::json!({
        "username": &username,
        "password": &password,
        "password_check": &password,
        "email": "testuser@example.com",
    });

    // Act
    app.post_signup(&body).await;

    // Assert
    let saved = sqlx::query!(
        "SELECT password_hash FROM users WHERE username=$1",
        username
    )
    .fetch_one(&app.pool)
    .await
    .expect("Failed to fetch users.");

    let expected_password_hash = PasswordHash::new(&saved.password_hash)
        .expect("Failed to parse password hash.");

    Argon2::default()
        .verify_password(password.as_bytes(), &expected_password_hash)
        .expect("Failed to verify password hash.");
}

#[tokio::test]
async fn password_must_match_password_check() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "totallydifferentpassword",
        "email": "testuser@example.com",
    });

    // Act
    let response = app.post_signup(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/signup");

    let html_page = app.get_signup_html().await;
    assert!(
        html_page.contains(r#"<span>You entered two different passwords - the field values must match.</span>"#)
    );
}

#[tokio::test]
async fn displays_warning_flash_message_if_unable_to_send_confirmation_email() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "password_check": "totallydifferentpassword",
        "email": "testuser@example.com",
    });

    // Act
    let response = app.post_signup(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/signup");

    let html_page = app.get_signup_html().await;
    assert!(
        html_page.contains(r#"<span>Failed to send confirmation email.</span>"#)
    );
}

use crate::helpers::{assert_is_redirect_to, spawn_app};
use serde_json::json;
use uuid::Uuid;

#[tokio::test]
async fn get_redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;
    let task_id = app.create_task().await;
    app.logout().await;

    // Act
    let response = app.get_edit_task_page(task_id).await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn post_redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;
    let task_id = app.create_task().await;
    app.logout().await;

    // Act
    let response = app
        .edit_task(task_id, json!({ "title": "new title" }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn post_returns_error_when_requested_to_delete_other_users_task() {
    // Arrange
    let app = spawn_app().await;

    wiremock::Mock::given(wiremock::matchers::path("/email"))
        .and(wiremock::matchers::method("POST"))
        .respond_with(wiremock::ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act part 1 - create a task as the test and then log out
    let task_id = app.create_task().await;
    app.logout().await;

    // Act part 2 - create a new user
    let username = String::from("seconduser");
    let password = Uuid::new_v4().to_string();
    let body = json!({
        "username": &username,
        "password": &password,
        "password_check": &password,
        "email": "test@example.com"
    });

    app.post_signup(&body)
        .await
        .error_for_status()
        .expect("Failed to create new user");

    // Act part 3 - log in as the new user and try to modify TestUser's task
    let body = json!({
        "username": &username,
        "password": &password,
    });
    app.post_login(&body)
        .await
        .error_for_status()
        .expect("Failed to log in");

    let body = json!({
        "title": "new title",
        "text": "lkjasda",
        "completed": true,
    });
    let response = app.edit_task(task_id, &body).await;

    // Assert
    assert_is_redirect_to(&response, "/tasks");
    let html_page = app.get_tasks_html().await;
    assert!(html_page.contains("You are not authorized to edit this task"));
}

#[tokio::test]
async fn post_successfully_modifies_task_upon_valid_request() {
    // Arrange
    let app = spawn_app().await;
    let task_id = app.create_task().await;

    // Act
    let body = json!({
        "title": "new title",
        "text": "new text",
        "completed": true,
    });
    let response = app.edit_task(task_id, &body).await;

    // Assert
    let task = app.get_tasks().await;
    let task = task.first().expect("Task missing?");

    assert_is_redirect_to(&response, "/tasks");
    assert_eq!(task.text, Some("new text".to_string()));
    assert_eq!(task.status, "complete");
    assert_eq!(task.title, "new title".to_string());
}

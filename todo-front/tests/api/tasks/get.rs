use crate::helpers::{assert_is_redirect_to, spawn_app};
use libtodo::domain::Task;
use serde_json::json;
use uuid::Uuid;

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app
        .api_client
        .get(format!("{}/tasks", &app.address))
        .send()
        .await
        .expect("Failed to execute request");

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn renders_tasks_for_valid_requests() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - log in
    let body = json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_login(&body).await;

    // Act part 2 - create a couple new tasks
    app.post_tasks(&json!({ "title": "Yahoo" })).await;
    app.post_tasks(&json!({ "title": "adasd" })).await;
    app.post_tasks(&json!({ "title": "adjasjd" })).await;

    // Act part 3 - get /tasks html
    let html_page = app.get_tasks_html().await;

    // Assert
    let saved = sqlx::query_as!(
        Task,
        r#"
        SELECT task_id, text, status, created_at, user_id, tags, title
        FROM tasks
        WHERE user_id = $1
        "#,
        &app.test_user.user_id
    )
    .fetch_all(&app.pool)
    .await
    .expect("Failed to get tasks from database");

    for task in saved {
        assert_contains_task(&html_page, task.task_id, &task.title);
    }
}

fn assert_contains_task(html: &str, task_id: Uuid, task_title: &str) {
    assert!(html.contains(&task_title));
    assert!(html.contains(&task_id.to_string()))
}
